(function($) {
	$(document).ready(function() {
		$('a.t3lephant-dump').click(function(){
			var url = 'ajax.php',
				data = {
					ajaxID: 'T3lephant::getRow',
					t3lephant: {
						table: $(this).data('table'),
						uid: $(this).data('uid')
					}
				};

			if ('undefined' !== typeof TYPO3.settings.ajaxUrls.t3lephant_getrow) {
				url = TYPO3.settings.ajaxUrls.t3lephant_getrow;
				delete data.ajaxID;
			}

			$.post(url, data)
			.done(function(response) {
				response = $.parseJSON(response);
				// Textarea with code
				var code = $('<textarea />')
						.attr('readonly', 'readonly')
						.text(response.data);

				// Copy button and clipboard functionality
				var copyButton = $('<button />').text('Copy').addClass('btn btn-primary copy-button');
				var clipboard = new Clipboard(copyButton.get(0), {
					target: function() {
						return code.get(0);
					}
				}).on('success', function(){
					$('<span />')
						.text('Code copied to clipboard!')
						.delay(1500)
						.fadeOut(function(){ $(this).remove(); })
						.insertAfter(copyButton);
				}).on('error', function(){
					alert('Not able to copy code to your clipboard. Please copy it manually.');
				});

				$('<div><h3>T3lephant Dump <small>[' + response.table + ':' + response.uid + ']</small></h3></div>')
					.append(code)
					.append(copyButton)
					.custommodal({
						zIndex: 300,
						modalClass: 't3lephant-modal'
					});
			})
			.fail(function(){
				alert('Could not fetch row. Did you configure a t3lephant data provider?');
			});
		});
	});
}(TYPO3.jQuery));
