
.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
t3lephant - data provider
=============================================================

t3lephant makes necessary system records in TYPO3, which are
required to be stored in database, deployable.


-------------------------------------------------------------
Documentation
-------------------------------------------------------------

The full documentation can be found in the wiki of t3lephant
on Bitbucket:

https://bitbucket.org/ArminVieweg/t3lephant/wiki/Home
