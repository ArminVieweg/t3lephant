<?php

return array(
    't3lephant_getrow' => array(
        'path' => '/t3lephant/get-row',
        'target' => 'ArminVieweg\T3lephant\Ajax\Dumper::getRow',
    )
);
