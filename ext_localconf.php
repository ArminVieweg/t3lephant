<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$boot = function ($extensionKey) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/class.db_list_extra.inc']['actions']['t3lephant_dump'] =
        'ArminVieweg\\T3lephant\\Hooks\\DumpButton';

    if (!\TYPO3\CMS\Core\Utility\GeneralUtility::compat_version('7.6') && TYPO3_MODE === 'BE') {
        // Register ajax route for TYPO3 < 7.6
        $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['AJAX']['T3lephant::getRow']
            = 'EXT:t3lephant/Classes/Ajax/Dumper.php:ArminVieweg\\T3lephant\\Ajax\\Dumper->getRow';
    }

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] =
        'ArminVieweg\\T3lephant\\Command\\T3lephantCommandController';

    $GLOBALS['TYPO3_CONF_VARS']['LOG'][$extensionKey]['writerConfiguration'] = array(
        \TYPO3\CMS\Core\Log\LogLevel::DEBUG => array(
            'ArminVieweg\\T3lephant\\Log\\Writer\\FileAndMemoryWriter' => array(
                'logFile' => 'typo3temp/logs/t3lephant.log'
            )
        )
    );
};

$boot($_EXTKEY);
unset($boot);
