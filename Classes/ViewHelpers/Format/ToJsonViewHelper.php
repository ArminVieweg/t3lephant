<?php
namespace ArminVieweg\T3lephant\ViewHelpers\Format;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Converts given array to json
 *
 * @package ArminVieweg\T3lephant
 */
class ToJsonViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Be\AbstractBackendViewHelper
{
    /**
     * Converts given array to json
     *
     * @param array|null $array Array which should be converted to json
     * @param bool $prettyPrint Outputs json well formatted, if supported in used php version
     * @return string JSON string
     */
    public function render(array $array = null, $prettyPrint = true)
    {
        if ($array === null) {
            $array = $this->renderChildren();
        }

        if ($prettyPrint && defined('JSON_PRETTY_PRINT')) {
            return json_encode($array, JSON_PRETTY_PRINT);
        }
        return json_encode($array);
    }
}
