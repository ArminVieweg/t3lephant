<?php
namespace ArminVieweg\T3lephant\ViewHelpers\Be\Version\Typo3;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2012-2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Checks if current TYPO3 version is 7.0 or greater
 *
 * @package ArminVieweg\T3lephant
 */
class Is7ViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Be\AbstractBackendViewHelper
{
    /**
     * Checks if current TYPO3 version is 7.0 or greater
     *
     * @return bool
     */
    public function render()
    {
        return \TYPO3\CMS\Core\Utility\GeneralUtility::compat_version('7.0');
    }
}
