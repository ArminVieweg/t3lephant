<?php
namespace ArminVieweg\T3lephant\ViewHelpers\Be\Version;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Returns version of T3lephant extension
 *
 * @package ArminVieweg\T3lephant
 */
class T3lephantViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Be\AbstractBackendViewHelper
{
    /**
     * Returns version of T3lephant extension
     *
     * @return bool
     */
    public function render()
    {
        return \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getExtensionVersion('t3lephant');
    }
}
