<?php
namespace ArminVieweg\T3lephant\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use ArminVieweg\T3lephant\Log\Writer\FileAndMemoryWriter;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Convenient wrapper class for t3lephant logger
 *
 * @package ArminVieweg\T3lephant
 */
class LoggerUtility
{
    /**
     * @var FileAndMemoryWriter
     */
    static protected $accessibleWriter;

    /**
     * Get logger
     *
     * @return \TYPO3\CMS\Core\Log\Logger
     */
    public static function get()
    {
        return GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Log\\LogManager')->getLogger('t3lephant');
    }

    /**
     * Returns instance of accessible writer.
     *
     * @return FileAndMemoryWriter|null
     */
    public static function getAccessibleWriter()
    {
        if (!self::$accessibleWriter instanceof FileAndMemoryWriter) {
            /** @var \TYPO3\CMS\Core\Log\Writer\WriterInterface $logWriter */
            foreach (self::get()->getWriters() as $logLevelWriters) {
                foreach ($logLevelWriters as $logWriter) {
                    if ($logWriter instanceof FileAndMemoryWriter) {
                        self::$accessibleWriter = $logWriter;
                        break;
                    }
                }
            }
        }
        return self::$accessibleWriter;
    }
}
