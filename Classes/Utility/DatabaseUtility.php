<?php
namespace ArminVieweg\T3lephant\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Database utility
 *
 * @package ArminVieweg\T3lephant
 */
class DatabaseUtility
{
    /**
     * Primary keys of database tables.
     * Key contains Table, Value the Primary Key as string
     * @var array
     */
    protected static $primaryKeys = array();

    /**
     * Returns a valid DatabaseConnection object that is connected and ready to be used static.
     *
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    public static function getDatabaseConnection()
    {
        if (!$GLOBALS['TYPO3_DB']) {
            \TYPO3\CMS\Core\Core\Bootstrap::getInstance()->initializeTypo3DbGlobal();
        }
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * Get primary key of given table
     *
     * @param string $tableName Name of table to get primary key column of
     * @return string Column name which contains the primary key (mostly "uid")
     */
    public static function getPrimaryKeyOfTable($tableName)
    {
        if (array_key_exists($tableName, self::$primaryKeys)) {
            return self::$primaryKeys[$tableName];
        }

        $tableKeys = self::getDatabaseConnection()->admin_get_keys($tableName);
        foreach ($tableKeys as $tableKey) {
            if ($tableKey['Key_name'] === 'PRIMARY') {
                self::$primaryKeys[$tableName] = $tableKey['Column_name'];
                return $tableKey['Column_name'];
            }
        }
    }
}
