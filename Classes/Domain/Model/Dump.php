<?php
namespace ArminVieweg\T3lephant\Domain\Model;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Dump model
 *
 * @package ArminVieweg\T3lephant
 */
class Dump
{
    /**
     * @var \SplFileObject
     */
    protected $file;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var \TYPO3\CMS\Core\Log\LogRecord[]
     */
    protected $validationErrors = array();

    /**
     * @param string $filePath
     * @param string|null $tableName
     * @return Dump
     */
    public function __construct($filePath, $tableName = null)
    {
        $file = new \SplFileObject($filePath);
        $this->setFile($file);
        if ($tableName) {
            $this->setTable($tableName);
        }
    }

    /**
     * Get File
     *
     * @return \SplFileObject
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set File
     *
     * @param \SplFileObject $file
     * @return void
     */
    public function setFile(\SplFileObject $file)
    {
        $this->file = $file;
    }

    /**
     * Returns the real path of dump file
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->getFile()->getRealPath();
    }

    /**
     * Returns file content
     *
     * @return string
     */
    public function getContents()
    {
        return file_get_contents($this->getFile()->getRealPath());
    }

    /**
     * Get Table
     *
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Set Table
     *
     * @param string $table
     * @return void
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * Get ValidationErrors
     *
     * @return \TYPO3\CMS\Core\Log\LogRecord[]
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * Add validation error for dump, using LogRecord.
     *
     * @param \TYPO3\CMS\Core\Log\LogRecord $logRecord
     * @return void
     */
    public function addValidationError(\TYPO3\CMS\Core\Log\LogRecord $logRecord)
    {
        $this->validationErrors[] = $logRecord;
    }
}
