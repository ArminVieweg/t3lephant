<?php
namespace ArminVieweg\T3lephant\Hooks;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use TYPO3\CMS\Backend\Utility\IconUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Export Button Hook for list view
 *
 * @package ArminVieweg\T3lephant
 */
class DumpButton implements \TYPO3\CMS\Recordlist\RecordList\RecordListHookInterface
{
    /**
     * @var \TYPO3\CMS\Core\Page\PageRenderer
     */
    protected static $pageRenderer;

    /**
     * Modifies Web>List clip icons (copy, cut, paste, etc.) of a displayed row
     *
     * @param string $table The current database table
     * @param array $row The current record row
     * @param array $cells The default clip-icons to get modified
     * @param \TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList $parentObject Instance of calling object
     * @return array The modified clip-icons
     */
    public function makeClip($table, $row, $cells, &$parentObject)
    {
        if ($this->getBackendUserAuthentication()->isAdmin()) {
            $extPath = ExtensionManagementUtility::extRelPath('t3lephant');
            $pageRenderer = $this->getPageRenderer();
            $pageRenderer->loadJquery();
            $pageRenderer->addJsLibrary('jqueryModal', $extPath . 'Resources/Public/JavaScript/jquery.modal.js');
            $pageRenderer->addJsLibrary('clipboard', $extPath . 'Resources/Public/JavaScript/clipboard.js');
            $pageRenderer->addJsFile($extPath . 'Resources/Public/JavaScript/DumpButton.js');

            $pageRenderer->addCssFile($extPath . 'Resources/Public/Css/T3lephant.css');
            if (!GeneralUtility::compat_version('7.6')) {
                $pageRenderer->addCssFile($extPath . 'Resources/Public/Css/T3lephant62.css');
            }

            if (\TYPO3\CMS\Core\Utility\GeneralUtility::compat_version('7.6')) {
                /** @var \TYPO3\CMS\Core\Imaging\IconFactory $iconFactory */
                $iconFactory = GeneralUtility::makeInstance('TYPO3\CMS\Core\Imaging\IconFactory');
                $icon = (string) $iconFactory->getIcon('ext-t3lephant-icon-16', \TYPO3\CMS\Core\Imaging\Icon::SIZE_SMALL);
            } else {
                $icon = IconUtility::getSpriteIcon('extensions-t3lephant-dump');
            }

            $cells['t3lephantDump'] = '<a href="#" class="t3lephant-dump btn btn-default" data-table="' . $table .
                '" data-uid="' . $row['uid'] . '">' . $icon . '</a>';
        }
        return $cells;
    }

    /**
     * Returns instance of PageRenderer to be able to influence styles and js in backend
     *
     * @return \TYPO3\CMS\Core\Page\PageRenderer
     */
    protected function getPageRenderer()
    {
        if (self::$pageRenderer instanceof \TYPO3\CMS\Core\Page\PageRenderer) {
            return self::$pageRenderer;
        }

        /** @var \TYPO3\CMS\Backend\Template\DocumentTemplate $mediumDocumentTemplate */
        $mediumDocumentTemplate = GeneralUtility::makeInstance('TYPO3\CMS\Backend\Template\DocumentTemplate');
        /** @var \TYPO3\CMS\Core\Page\PageRenderer $pr */
        self::$pageRenderer = $mediumDocumentTemplate->getPageRenderer();
        return self::$pageRenderer;
    }

    /**
     * Returns current backend user
     *
     * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected function getBackendUserAuthentication()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     * Modifies Web>List control icons of a displayed row
     * Unused but required because of interface.
     *
     * @param string $table The current database table
     * @param array $row The current record row
     * @param array $cells The default control-icons to get modified
     * @param object $parentObject Instance of calling object
     * @return array The modified control-icons
     */
    public function makeControl($table, $row, $cells, &$parentObject)
    {
        return $cells;
    }

    /**
     * Modifies Web>List header row columns/cells
     * Unused but required because of interface.
     *
     * @param string $table The current database table
     * @param array $currentIdList Array of the currently displayed uids of the table
     * @param array $headerColumns An array of rendered cells/columns
     * @param object $parentObject Instance of calling (parent) object
     * @return array Array of modified cells/columns
     */
    public function renderListHeader($table, $currentIdList, $headerColumns, &$parentObject)
    {
        return $headerColumns;
    }

    /**
     * Modifies Web>List header row clipboard/action icons
     * Unused but required because of interface.
     *
     * @param string $table The current database table
     * @param array $currentIdList Array of the currently displayed uids of the table
     * @param array $cells An array of the current clipboard/action icons
     * @param object $parentObject Instance of calling (parent) object
     * @return array Array of modified clipboard/action icons
     */
    public function renderListHeaderActions($table, $currentIdList, $cells, &$parentObject)
    {
        return $cells;
    }
}
