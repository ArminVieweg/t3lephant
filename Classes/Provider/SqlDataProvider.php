<?php
namespace ArminVieweg\T3lephant\Provider;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Json Data Provider
 *
 * @package ArminVieweg\T3lephant
 */
class SqlDataProvider extends AbstractDataProvider implements DataProviderInterface
{
    public function import()
    {
    }

    public function export(array $row)
    {
    }

    public function validateDumps()
    {
    }

    public function compare()
    {
    }
}
