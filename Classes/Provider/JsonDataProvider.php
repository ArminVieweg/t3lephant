<?php
namespace ArminVieweg\T3lephant\Provider;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use ArminVieweg\T3lephant\Utility\DatabaseUtility;
use ArminVieweg\T3lephant\Utility\LoggerUtility;

/**
 * Json Data Provider
 *
 * @package ArminVieweg\T3lephant
 */
class JsonDataProvider extends AbstractDataProvider implements DataProviderInterface
{
    /**
     * @var array
     */
    protected $allowedFilePatterns = array('*.json');

    /**
     * Performs the import
     *
     * @return bool Status of import
     */
    public function import()
    {
        if (!$this->validateDumps()) {
            return false;
        }

        $databaseConnection = DatabaseUtility::getDatabaseConnection();
        $databaseConnection->store_lastBuiltQuery = true;

        $hasErrors = false;
        foreach ($this->getDumpsFromConfiguration(false) as $dump) {
            $dumpRows = json_decode($dump->getContents(), true);
            foreach ($dumpRows as $dumpRow) {
                // First try to insert record
                $insertStatus = $databaseConnection->exec_INSERTquery(
                    $dump->getTable(),
                    $dumpRow
                );

                $updateStatus = true;
                if (!$insertStatus) {
                    $failedInsertQuery = $databaseConnection->debug_lastBuiltQuery;

                    $primaryKey = DatabaseUtility::getPrimaryKeyOfTable($dump->getTable());
                    $primaryKeyValue = (int) $dumpRow[$primaryKey];

                    if ($primaryKeyValue > 0) {
                        // If inserting the row fails, try to update record
                        $updateStatus = $databaseConnection->exec_UPDATEquery(
                            $dump->getTable(),
                            $primaryKey . '=' . $primaryKeyValue,
                            $dumpRow
                        );
                    }
                } else {
                    LoggerUtility::get()->debug('Successful INSERT query', array(
                        'query' => $databaseConnection->debug_lastBuiltQuery
                    ));
                }

                if (!$insertStatus && !$updateStatus) {
                    $failedUpdateQuery = $databaseConnection->debug_lastBuiltQuery;
                    LoggerUtility::get()->error(
                        'SQL queries failed!',
                        array(
                            'insertQuery' => $failedInsertQuery,
                            'updateQuery' => $failedUpdateQuery,
                        )
                    );
                    $hasErrors = true;
                } else {
                    LoggerUtility::get()->debug('Successful query', array(
                        'query' => $databaseConnection->debug_lastBuiltQuery
                    ));
                }
            }
        }
        return !$hasErrors;
    }

    /**
     * Exports given row to json. If PHP 5.4+ the output is prettyfied.
     *
     * @param array $row
     * @return string json
     */
    public function export(array $row)
    {
        if (defined('JSON_PRETTY_PRINT')) {
            return json_encode($row, JSON_PRETTY_PRINT);
        }
        return json_encode($row);
    }

    /**
     * Checks if previously added dumps are valid.
     *
     * @return bool
     * @TODO REFACTOR!
     */
    public function validateDumps()
    {
        $dumps = $this->getDumpsFromConfiguration();
        if (empty($dumps)) {
            LoggerUtility::get()->error('No dumps found! Did you configure any?');
            return false;
        }

        $databaseConnection = DatabaseUtility::getDatabaseConnection();

        $hasError = false;
        foreach ($dumps as $dump) {
            // Check if table exists
            if (!in_array($dump->getTable(), array_keys($databaseConnection->admin_get_tables()))) {
                LoggerUtility::get()->error('Table not existing', array(
                    'fileName' => $dump->getFile()->getRealPath(),
                    'tableName' => $dump->getTable()
                ));
                $hasError = true;
            }

            $jsonContent = json_decode($dump->getContents(), true);

            // Check if json is valid
            if ($jsonContent === null) {
                LoggerUtility::get()->error('No valid JSON given.', array(
                    'fileName' => $dump->getFile()->getRealPath()
                ));
                $hasError = true;
            }

            // Check if json contains array
            if (!is_numeric(array_keys($jsonContent)[0])) {
                LoggerUtility::get()->error(
                    'JSON is valid but it does not start with an array. Please wrap your contents with [ ].',
                    array(
                        'fileName' => $dump->getFile()->getRealPath()
                    )
                );
                $hasError = true;
            }

            if ($hasError) {
                return !$hasError;
            }

            // Check json content
            $fields = $databaseConnection->admin_get_fields($dump->getTable());
            $fieldsArray = array_keys($fields);

            $requiredFields = array();
            foreach ($fields as $name => $fieldConfiguration) {
                if ($fieldConfiguration['Null'] === 'NO' && $fieldConfiguration['Default'] === null) {
                    $requiredFields[] = $name;
                }
            }

            foreach ($jsonContent as $jsonObject) {
                $jsonObjectKeys = array_keys($jsonObject);

                // Check if required attribute is missing
                $missingAttributes = array_diff($requiredFields, $jsonObjectKeys);
                if (!empty($missingAttributes)) {
                    LoggerUtility::get()->error(
                        'Some fields which are required in database are missing in your JSON.',
                        array(
                            'fileName' => $dump->getFile()->getRealPath(),
                            'tableName' => $dump->getTable(),
                            'missingAttributes' => array_values($missingAttributes)
                        )
                    );
                    $hasError = true;
                }

                // Check if json contains unused attributes. This is just a notice.
                $unusedAttributes = array_diff($jsonObjectKeys, $fieldsArray);
                if (!empty($unusedAttributes)) {
                    LoggerUtility::get()->error('Your JSON contains attributes, which are not existing in DB.', array(
                        'fileName' => $dump->getFile()->getRealPath(),
                        'tableName' => $dump->getTable(),
                        'notExistingAttributes' => array_values($unusedAttributes)
                    ));
                    $hasError = true;
                }
            }
        }
        return !$hasError;
    }

    /**
     * Compares given dumps with database contents and return differences.
     *
     * When dumps are not valid this method returns false. When database is fully
     * in sync with given dumps it returns empty array, otherwise the differences in array.
     *
     * @return bool|array
     */
    public function compare()
    {
        if (!$this->validateDumps()) {
            return false;
        }
        $databaseConnection = DatabaseUtility::getDatabaseConnection();
        $differences = array();

        foreach ($this->getDumpsFromConfiguration(false) as $dump) {
            $dumpRows = json_decode($dump->getContents(), true);
            foreach ($dumpRows as $dumpRow) {
                $primaryKey = DatabaseUtility::getPrimaryKeyOfTable($dump->getTable());
                $status = 'update';
                $databaseRow = $databaseConnection->exec_SELECTgetSingleRow(
                    implode(',', array_keys($dumpRow)),
                    $dump->getTable(),
                    $primaryKey . ' = ' . intval($dumpRow[$primaryKey])
                );

                if (!$databaseRow) {
                    $status = 'new';
                    $databaseRow = array();
                }

                $rowDifferences = array_diff($dumpRow, $databaseRow);
                if (!empty($rowDifferences)) {
                    $differences[] = array(
                        'dump' => $dump,
                        'dumpRow' => $dumpRow,
                        'dumpPrimaryKey' => $primaryKey,
                        'dumpPrimaryKeyValue' => intval($dumpRow[$primaryKey]),
                        'affectedDatabaseValues' => array_intersect_key($databaseRow, $rowDifferences),
                        'fullDatabaseRow' => $databaseRow,
                        'diff' => $rowDifferences,
                        'status' => $status
                    );
                }
            }
        }
        return $differences;
    }
}
