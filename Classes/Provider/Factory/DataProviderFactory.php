<?php
namespace ArminVieweg\T3lephant\Provider\Factory;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Data Provider Factory
 *
 * @package ArminVieweg\T3lephant
 */
class DataProviderFactory implements \TYPO3\CMS\Core\SingletonInterface
{
    const JSON_PROVIDER = 'ArminVieweg\\T3lephant\\Provider\\JsonDataProvider';
    const SQL_PROVIDER = 'ArminVieweg\\T3lephant\\Provider\\SqlDataProvider';

    /**
     * Creates new instance of requested DataProvider
     *
     * @param string $className Class must exists. Check class constants for available values
     * @return \ArminVieweg\T3lephant\Provider\DataProviderInterface
     * @throws \Exception
     */
    public static function create($className)
    {
        static::includeRequiredFiles();
        static::prepareConfigurationStorage();

        if (!class_exists($className)) {
            throw new \Exception(
                'Class "' . $className . '" not existing. Unable to get t3lephant data provider!'
            );
        }
        return \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance($className);
    }

    /**
     * Returns instance of configured DataProvider. If this method is called without
     * a configured t3lephant data provider, it will throw exceptions.
     *
     * @return \ArminVieweg\T3lephant\Provider\DataProviderInterface
     * @throws \Exception
     */
    public static function get()
    {
        if (!isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['activeProvider'])) {
            throw new \Exception('No active active provider given, for t3lephant! Please check your configuration!');
        }
        $activeProviderClassName = $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['activeProvider'];
        if (!class_exists($activeProviderClassName)) {
            throw new \Exception(
                'Class "' . $activeProviderClassName . '" not existing. Unable to get t3lephant data provider!'
            );
        }
        return \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance($activeProviderClassName);
    }


    /**
     * Includes the required files, if necessary
     *
     * @return void
     */
    protected static function includeRequiredFiles()
    {
        if (!class_exists('ArminVieweg\T3lephant\Provider\DataProviderInterface')) {
            require_once(PATH_typo3conf . 'ext/t3lephant/Classes/Provider/DataProviderInterface.php');
        }
        if (!class_exists('ArminVieweg\T3lephant\Provider\AbstractDataProvider')) {
            require_once(PATH_typo3conf . 'ext/t3lephant/Classes/Provider/AbstractDataProvider.php');
        }
        if (!class_exists('ArminVieweg\T3lephant\Provider\JsonDataProvider')) {
            require_once(PATH_typo3conf . 'ext/t3lephant/Classes/Provider/JsonDataProvider.php');
        }
        if (!class_exists('ArminVieweg\T3lephant\Provider\SqlDataProvider')) {
            require_once(PATH_typo3conf . 'ext/t3lephant/Classes/Provider/SqlDataProvider.php');
        }
    }

    /**
     * Dump files/directories are stored in $GLOBALS['TYPO3_CONF_VARS']['EXT'].
     * This method initializes valid arrays in expected destinations.
     *
     * @return void
     */
    protected static function prepareConfigurationStorage()
    {
        if (!isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['activeProvider'])) {
            $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['activeProvider'] = null;
        }
        if (!is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['registredDumps'])) {
            $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['registredDumps'] = array();
        }
        if (!is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['registredDumpDirectories'] = array())) {
            $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['registredDumpDirectories'] = array();
        }
    }
}
