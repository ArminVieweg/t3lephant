<?php
namespace ArminVieweg\T3lephant\Provider;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use ArminVieweg\T3lephant\Domain\Model\Dump;
use ArminVieweg\T3lephant\Utility\LoggerUtility;

/**
 * Abstract Data Provider
 *
 * @package ArminVieweg\T3lephant
 */
abstract class AbstractDataProvider
{
    /**
     * Used by php function fnmatch. Case insensitive.
     * @var array
     */
    protected $allowedFilePatterns = array('*');

    /**
     * Set active provider
     *
     * @return AbstractDataProvider
     */
    public function __construct()
    {
        $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['activeProvider'] = get_class($this);
    }

    /**
     * Add dump file to configuration
     *
     * @param string $fileName
     * @param string|null $tableName
     * @return void
     */
    public function addDumpFile($fileName, $tableName = null)
    {
        $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['registredDumps'][] = array(
            'fileName' => $fileName,
            'tableName' => $tableName
        );
    }

    /**
     * Add dump directory to configuration
     *
     * @param string $directoryName
     * @param bool $recursive
     * @return void
     */
    public function addDumpDirectory($directoryName, $recursive = false)
    {
        $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['registredDumpDirectories'][] = array(
            'directoryName' => $directoryName,
            'recursive' => $recursive
        );
    }

    /**
     * Returns added dumps
     *
     * @return array
     */
    protected function getFileConfiguration()
    {
        return $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['registredDumps'];
    }

    /**
     * Returns added dump directories
     *
     * @return array
     */
    protected function getDirectoryConfiguration()
    {
        return $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3lephant']['registredDumpDirectories'];
    }

    /**
     * Get list of dump files based on configuration.
     *
     * @param bool $logNotices
     * @return Dump[] With realPath of file as key
     */
    public function getDumpsFromConfiguration($logNotices = true)
    {
        $dumps = array_merge(
            array(),
            $this->getDumpsFromDirectoryConfiguration($this->getDirectoryConfiguration()),
            $this->getDumpsFromFileConfiguration($this->getFileConfiguration(), $logNotices)
        );

        $dumpsWithRealPathAsKey = array();
        /** @var Dump $dump */
        foreach ($dumps as $dump) {
            $dumpsWithRealPathAsKey[$dump->getFilePath()] = $dump;
        }
        return $dumpsWithRealPathAsKey;
    }

    /**
     * Checks if given file name is matching at least one of the defined $this->allowedFilePatterns
     *
     * @param string $fileName
     * @return bool
     */
    protected function checkFileNamePatterns($fileName)
    {
        foreach ($this->allowedFilePatterns as $allowedFilePattern) {
            $blah = fnmatch($allowedFilePattern, $fileName, FNM_CASEFOLD);
            if ($blah) {
                return true;
            }
        }
        return false;
    }

    /**
     * Extracts from given file name the table name.
     * This method may get overwritten by DataProviders to fit their requirements.
     *
     * This method in AbstractDataProvider just returns the file name without file extension
     * as table name.
     *
     * @param string $fileName
     * @return string
     */
    protected function getTableNameFromFileName($fileName)
    {
        $pathParts = pathinfo($fileName);
        return $pathParts['filename'];
    }

    /**
     * Create Dump instances for found files in configured directories
     *
     * @param array $directoryConfiguration
     * @return Dump[]
     */
    protected function getDumpsFromDirectoryConfiguration(array $directoryConfiguration)
    {
        $dumps = array();
        foreach ($directoryConfiguration as $dumpDirectory) {
            $validPathToDumpDirectory = realpath($dumpDirectory['directoryName']);
            if (!$validPathToDumpDirectory) {
                continue;
            }

            $filesInDirectory = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator(
                    $validPathToDumpDirectory,
                    \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::FOLLOW_SYMLINKS
                )
            );
            if (!$dumpDirectory['recursive']) {
                $filesInDirectory->setMaxDepth(0);
            }

            /** @var \SplFileInfo $file */
            foreach ($filesInDirectory as $file) {
                if (!$file->isFile() || !$this->checkFileNamePatterns($file->getRealPath())) {
                    continue;
                }
                $dumps[$file->getRealPath()] = new Dump($file->getRealPath(), $this->getTableNameFromFileName($file));
            }
        }
        return $dumps;
    }

    /**
     * Create Dump instances for configured files
     *
     * @param array $fileConfiguration
     * @param bool $logNotices
     * @return Dump[]
     */
    protected function getDumpsFromFileConfiguration(array $fileConfiguration, $logNotices = true)
    {
        $dumps = array();
        foreach ($fileConfiguration as $dumpFile) {
            if (!$this->checkFileNamePatterns($dumpFile['fileName'])) {
                if ($logNotices) {
                    LoggerUtility::get()->notice(
                        'Skipped file because it does not match allowed file patterns.',
                        array(
                            'fileName' => $dumpFile['fileName'],
                            'allowedFilePatterns' => $this->allowedFilePatterns
                        )
                    );
                }
                continue;
            }
            $validPathToExistingDumpFile = realpath($dumpFile['fileName']);
            if (!$validPathToExistingDumpFile) {
                if ($logNotices) {
                    LoggerUtility::get()->notice(
                        'Skipped file because it is not existing. Please check your configuration.',
                        array(
                            'fileName' => $dumpFile['fileName']
                        )
                    );
                }
                continue;
            }

            $tableName = $dumpFile['tableName'];
            if (empty($tableName)) {
                $tableName = $this->getTableNameFromFileName($validPathToExistingDumpFile);
            }
            $dumps[$validPathToExistingDumpFile] = new Dump($validPathToExistingDumpFile, $tableName);
        }
        return $dumps;
    }
}
