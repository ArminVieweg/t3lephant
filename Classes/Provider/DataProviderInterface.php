<?php
namespace ArminVieweg\T3lephant\Provider;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Interface for t3lephant data providers
 *
 * @package ArminVieweg\T3lephant
 */
interface DataProviderInterface extends \TYPO3\CMS\Core\SingletonInterface
{

    public function import();

    /**
     * Converts given database row to string which can be stored in files.
     *
     * @param array $row
     * @return string
     */
    public function export(array $row);

    /**
     * Checks if previously added dumps are valid.
     *
     * @return bool
     */
    public function validateDumps();

    /**
     * Compares given dumps with database contents and return differences.
     *
     * When dumps are not valid this method returns false. When database is fully
     * in sync with given dumps it returns empty array, otherwise the differences in array.
     *
     * @return bool|array
     */
    public function compare();

    /**
     * Add dump file to t3lephant configuration. If validation fails
     * sys_log entry will be generated.
     *
     * @param string $fileName Absolute path to dump file
     * @param string|null $tableName Optional tableName, if not provided by file name
     * @return void
     */
    public function addDumpFile($fileName, $tableName = null);

    /**
     * Add directory containing dump files to t3lephant configuration. If validation fails
     * sys_log entry will be generated.
     *
     * @param string $directoryName Absolute path to directory containing dump files
     * @param bool $recursive Default set to false. If true subfolders will also be respected
     * @return void
     */
    public function addDumpDirectory($directoryName, $recursive = false);

    /**
     * Get list of dump files based on configuration.
     *
     * @return \ArminVieweg\T3lephant\Domain\Model\Dump[]
     */
    public function getDumpsFromConfiguration();
}
