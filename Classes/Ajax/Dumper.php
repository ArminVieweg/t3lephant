<?php
namespace ArminVieweg\T3lephant\Ajax;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use ArminVieweg\T3lephant\Utility\DatabaseUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * AJAX Dumper
 *
 * @package ArminVieweg\T3lephant
 */
class Dumper
{
    /**
     * @var \ArminVieweg\T3lephant\Provider\Factory\DataProviderFactory
     */
    protected $dataProviderFactory;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dataProviderFactory = GeneralUtility::makeInstance(
            'ArminVieweg\T3lephant\Provider\Factory\DataProviderFactory'
        );
    }

    /**
     * Returns row of selected record in current DataProvider format
     *
     * @return string
     */
    public function getRow()
    {
        $parameter = $this->getParameter();
        $dataProvider = $this->dataProviderFactory->get();

        $row = DatabaseUtility::getDatabaseConnection()->exec_SELECTgetSingleRow(
            '*',
            $parameter['table'],
            'uid=' . (int) $parameter['uid']
        );

        $output = array(
            'data' => $dataProvider->export($row),
            'table' => $parameter['table'],
            'uid' => (int) $parameter['uid']
        );

        if (defined('JSON_PRETTY_PRINT')) {
            echo json_encode($output, JSON_PRETTY_PRINT);
        } else {
            echo json_encode($output);
        }
    }

    /**
     * @return array
     */
    protected function getParameter()
    {
        return GeneralUtility::_GPmerged('t3lephant');
    }
}
