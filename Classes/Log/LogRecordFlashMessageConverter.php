<?php
namespace ArminVieweg\T3lephant\Log;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Messaging\FlashMessage;

/**
 * Converter for LogRecords to FlashMessages
 *
 * @package ArminVieweg\T3lephant
 */
class LogRecordFlashMessageConverter
{
    /**
     * Log level mappings to flash message severities
     * @var array
     */
    protected static $severityMappings = array(
        LogLevel::DEBUG => FlashMessage::INFO,
        LogLevel::INFO => FlashMessage::INFO,
        LogLevel::NOTICE => FlashMessage::NOTICE,
        LogLevel::WARNING => FlashMessage::WARNING,
        LogLevel::ALERT => FlashMessage::WARNING,
        LogLevel::ERROR => FlashMessage::ERROR,
        LogLevel::CRITICAL => FlashMessage::ERROR,
        LogLevel::EMERGENCY => FlashMessage::ERROR
    );

    /**
     * Log level mappings to strings, which will be shown as flash message title
     * @var array
     */
    protected static $namedLogLevels = array(
        LogLevel::INFO => 'Info',
        LogLevel::NOTICE => 'Notice',
        LogLevel::WARNING => 'Warning',
        LogLevel::ALERT => 'Alert',
        LogLevel::ERROR => 'Error',
        LogLevel::CRITICAL => 'Critical',
        LogLevel::EMERGENCY => 'Emergency'
    );

    /**
     * Converts given log record to flash message
     *
     * @param \TYPO3\CMS\Core\Log\LogRecord $logRecord
     * @return FlashMessage
     */
    public static function convert(\TYPO3\CMS\Core\Log\LogRecord $logRecord)
    {
        $logRecordData = $logRecord->getData();
        $data = '';
        if (!empty($logRecordData)) {
            if (isset($logRecordData['exception']) && $logRecordData['exception'] instanceof \Exception) {
                $logRecordData['exception'] = (string)$logRecordData['exception'];
            }
            if (defined('JSON_PRETTY_PRINT')) {
                $data = nl2br(
                    PHP_EOL . str_replace('  ', ' &nbsp;&nbsp;', json_encode($logRecordData, JSON_PRETTY_PRINT))
                );
            } else {
                $data = PHP_EOL . json_encode($logRecordData);
            }
        }

        return new FlashMessage(
            $logRecord->getMessage() . $data,
            self::$namedLogLevels[$logRecord->getLevel()],
            self::$severityMappings[$logRecord->getLevel()]
        );
    }
}
