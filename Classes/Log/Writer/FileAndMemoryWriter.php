<?php
namespace ArminVieweg\T3lephant\Log\Writer;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Extends TYPO3's file writer for logging.
 * LogEntries for current request are directly accessible for further handling.
 *
 * @package ArminVieweg\T3lephant
 *
 * @TODO: Write a StorageWriter instead, which can be configured separately from file logger
 */
class FileAndMemoryWriter extends \TYPO3\CMS\Core\Log\Writer\FileWriter
{
    /**
     * @var \TYPO3\CMS\Core\Log\LogRecord[]
     */
    protected $logRecords = array();

    /**
     * Calls parent writeLog() method and stores LogRecords in variable
     *
     * @param \TYPO3\CMS\Core\Log\LogRecord $record
     * @return FileAndMemoryWriter
     */
    public function writeLog(\TYPO3\CMS\Core\Log\LogRecord $record)
    {
        parent::writeLog($record);
        array_unshift($this->logRecords, $record);
        return $this;
    }

    /**
     * Returns log records created in this request
     *
     * @return \TYPO3\CMS\Core\Log\LogRecord[]
     */
    public function getLogRecords()
    {
        return $this->logRecords;
    }
}
