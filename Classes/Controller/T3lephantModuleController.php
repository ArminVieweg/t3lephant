<?php
namespace ArminVieweg\T3lephant\Controller;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use ArminVieweg\T3lephant\Utility\LoggerUtility;

/**
 * T3lephant Module Controller
 *
 * @package ArminVieweg\T3lephant
 */
class T3lephantModuleController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \ArminVieweg\T3lephant\Provider\Factory\DataProviderFactory
     * @inject
     */
    protected $dataProviderFactory;


    /**
     * Index Action
     *
     * @return void
     */
    public function indexAction()
    {
        try {
            $dataProvider = $this->dataProviderFactory->get();
        } catch (\Exception $e) {
            $this->addFlashMessage($e->getMessage(), '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
            return;
        }

        $dumps = $dataProvider->getDumpsFromConfiguration();
        $validationStatus = $dataProvider->validateDumps();
        if (!$validationStatus) {
            /** @var \TYPO3\CMS\Core\Log\LogRecord $logRecord */
            foreach (LoggerUtility::getAccessibleWriter()->getLogRecords() as $logRecord) {
                if (isset($dumps[$logRecord->getData()['fileName']])) {
                    /** @var \ArminVieweg\T3lephant\Domain\Model\Dump $dump */
                    $dump = $dumps[$logRecord->getData()['fileName']];
                    $dump->addValidationError($logRecord);
                }
            }
        }
        $this->view->assign('validationStatus', $validationStatus);
        $this->view->assign('dumps', $dumps);

        if ($validationStatus) {
            $dataCompareResult = $dataProvider->compare();
            $this->view->assign('dataCompareResult', $dataCompareResult);
        }

        $this->view->assign('dataProvider', $dataProvider);
        $this->view->assign('dataProviderClass', get_class($dataProvider));
    }

    /**
     * Import Action
     *
     * @return void
     */
    public function importAction()
    {
        $dataProvider = $this->dataProviderFactory->get();
        $status = $dataProvider->import();
        if ($status) {
            $this->addFlashMessage('Import completed.', 'Success');
        } else {
            $this->addFlashMessage(
                'The import failed. Please check log.',
                'Failure',
                \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
            );
        }

        $this->forward('index');
    }
}
