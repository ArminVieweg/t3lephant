<?php
namespace ArminVieweg\T3lephant\Command;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use ArminVieweg\T3lephant\Log\LogRecordFlashMessageConverter;
use ArminVieweg\T3lephant\Utility\LoggerUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\CommandException;

/**
 * T3lephant Command Controller
 *
 * @package ArminVieweg\T3lephant
 */
class T3lephantCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController
{
    /**
     * @var \ArminVieweg\T3lephant\Provider\Factory\DataProviderFactory
     * @inject
     */
    protected $dataProviderFactory;

    /**
     * Import Command
     *
     * @return void
     * @throws CommandException
     */
    public function importCommand()
    {
        $dataProvider = $this->dataProviderFactory->get();
        $status = $dataProvider->import();

        if ($status) {
            $cliStatus = '[OK]';
            $message = new FlashMessage('Import completed.', 'Success');
        } else {
            $cliStatus = '[ERR]';
            $message = new FlashMessage(
                'The import failed. Please check flash messages or log.',
                'Failure',
                FlashMessage::ERROR
            );
        }
        $this->handleCommandOutput($message, $cliStatus);
    }

    /**
     * Validates configured dumps
     *
     * @return void
     * @throws CommandException
     */
    public function validateDumpsCommand()
    {
        $dataProvider = $this->dataProviderFactory->get();
        $isValid = $dataProvider->validateDumps();

        if ($isValid) {
            $cliStatus = '[OK]';
            $message = new FlashMessage('The current t3lephant configuration is valid. Good job!', 'Success');
        } else {
            $cliStatus = '[ERR]';
            $message = new FlashMessage(
                'The current t3lephant configuration is not valid! Please check flash messages or log.',
                'Failure',
                FlashMessage::WARNING
            );
        }
        $this->handleCommandOutput($message, $cliStatus);
    }

    /**
     * Compares dump contents with database
     *
     * @return void
     * @throws CommandException
     */
    public function compareDumpsWithDatabaseCommand()
    {
        $dataProvider = $this->dataProviderFactory->get();
        $dataCompareResult = $dataProvider->compare();

        if ($dataCompareResult === false) {
            // Validation failed
            $cliStatus = '[ERR]';
            $message = new FlashMessage(
                'The current t3lephant configuration is not valid! Please check flash messages or log.',
                'Failure',
                FlashMessage::WARNING
            );
        } else {
            if (empty($dataCompareResult)) {
                $cliStatus = '[OK]';
                $message = new FlashMessage(
                    'Everything is up to date!',
                    'Success',
                    FlashMessage::OK
                );
            } else {
                $cliStatus = '[WAR]';
                $message = new FlashMessage(
                    count($dataCompareResult) . ' row(s) affected.',
                    'Database is not in sync with defined dump contents.',
                    FlashMessage::WARNING
                );
            }
        }
        $this->handleCommandOutput($message, $cliStatus);
    }

    /**
     * Handles output of commands (for CLI and scheduler calls)
     *
     * @param FlashMessage $message
     * @param string $cliStatus
     * @return void
     */
    protected function handleCommandOutput(FlashMessage $message, $cliStatus)
    {
        if (defined('TYPO3_cliMode')) {
            if (!empty($message->getTitle())) {
                $this->outputLine($cliStatus . ' ' . $message->getTitle());
                $this->outputLine($message->getMessage());
            } else {
                $this->outputLine($cliStatus . ' ' . $message->getMessage());
            }
        }
        $this->addFlashMessage($message);

        foreach (LoggerUtility::getAccessibleWriter()->getLogRecords() as $logRecord) {
            $this->addFlashMessage(LogRecordFlashMessageConverter::convert($logRecord));
        }
    }

    /**
     * Adds flash message
     *
     * @param FlashMessage $message
     * @return void
     */
    protected function addFlashMessage(FlashMessage $message)
    {
        /** @var $flashMessageService \TYPO3\CMS\Core\Messaging\FlashMessageService */
        $flashMessageService = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessageService');
        /** @var $defaultFlashMessageQueue \TYPO3\CMS\Core\Messaging\FlashMessageQueue */
        $defaultFlashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();
        $defaultFlashMessageQueue->enqueue($message);
    }
}
