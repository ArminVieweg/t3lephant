# t3lephant for TYPO3

t3lephant makes necessary system records in TYPO3, which are
required to be stored in database, deployable.


## Documentation

The full documentation can be found in the wiki of t3lephant
on Bitbucket:

https://bitbucket.org/ArminVieweg/t3lephant/wiki/Home


## Build status

|master|develop|
|---------|----------|
|[![t3lephant Master branch](http://ci.v.ieweg.de/build-status/image/9?branch=master)](http://ci.v.ieweg.de/build-status/view/9?branch=master)|[![t3lephant Develop branch](http://ci.v.ieweg.de/build-status/image/9?branch=develop)](http://ci.v.ieweg.de/build-status/view/9?branch=develop)|
