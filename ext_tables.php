<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2015 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$boot = function ($extensionKey) {
    if (\TYPO3\CMS\Core\Utility\GeneralUtility::compat_version('7.6')) {
        // Add sprite icon (7.6)
        /** @var \TYPO3\CMS\Core\Imaging\IconRegistry $iconRegistry */
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Imaging\IconRegistry');
        // DCE Type Icons
        $iconRegistry->registerIcon(
            'ext-t3lephant-icon-16',
            'TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider',
            array('source' => 'EXT:t3lephant/Resources/Public/Icons/t3lephant-16.png')
        );
    } else {
        // Add sprite icon (6.2)
        \TYPO3\CMS\Backend\Sprite\SpriteManager::addSingleIcons(
            array(
                'dump' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extensionKey) .
                    'Resources/Public/Icons/t3lephant-16.png'
            ),
            $extensionKey
        );
    }

    // Register backend module
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'ArminVieweg.' . $extensionKey,
        'tools',
        't3lephantModule',
        '',
        array(
            'T3lephantModule' => 'index,import'
        ),
        array(
            'access' => 'user,group',
            'icon' => 'EXT:' . $extensionKey . '/ext_icon.png',
            'labels' => 'LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf',
        )
    );

};

$boot($_EXTKEY);
unset($boot);
